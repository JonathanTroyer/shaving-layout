# shaving-layout

Shave down your key travel. Inspired by [yak-layout](https://github.com/wincent/yak-layout).

## Additional references
[carpalx](http://mkweb.bcgsc.ca/carpalx/)

[The White keyboard layout](https://github.com/mw8/white_keyboard_layout), which is another heavy inspiration for the thought process behind this project.

Paul Guerin's [excellent article](https://paulguerin.medium.com/the-search-for-the-worlds-best-keyboard-layout-98d61b33b8e1) about his search for the best keyboard layout
