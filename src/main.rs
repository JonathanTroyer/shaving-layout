mod types;

use clap::Parser;
use indicatif::ProgressBar;
use indicatif::ProgressStyle;
use log::warn;
use rand::prelude::*;
use rayon::prelude::*;
use std::collections::HashSet;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;
use types::constraints::Pairing;
use types::layout::Layout;
use types::layout::LayoutTraits;
use types::weights::*;

#[derive(Parser, Debug)]
#[command(name = "shave")]
struct Args {
    #[arg(
        short,
        long,
        default_value = "1000",
        help = "How many layouts in each generation",
        display_order = 1
    )]
    population: usize,

    #[arg(
        short,
        long,
        default_value = "10000",
        help = "How many generations to iterate",
        display_order = 2
    )]
    generations: usize,

    #[arg(
        short,
        long,
        default_value = "20",
        help = "How many layouts to keep between generations",
        display_order = 3
    )]
    keep_top_n: usize,

    #[arg(
        short = 'c',
        long,
        default_value = "0.02",
        help = "Percentage chance to introduce a mutation when creating a layout",
        display_order = 4
    )]
    permutation_chance: f64,

    #[arg(
        short,
        long,
        default_value = "2",
        help = "How many layers the keyboard has (shift, fn, etc.)",
        display_order = 5
    )]
    layers: usize,

    #[arg(
        long,
        default_value = "data/key_weights.json",
        help = "Path to key weights",
        display_order = 6
    )]
    key_weights: String,

    #[arg(
        long,
        default_value = "data/char_weights.json",
        help = "Path to character weights",
        display_order = 7
    )]
    char_weights: String,

    #[arg(
        long,
        default_value = "data/ngram_weights.json",
        help = "Path to ngram weights",
        display_order = 8
    )]
    ngram_weights: String,

    #[arg(
        long,
        default_value = "data/pairings.json",
        help = "Path to pairings",
        display_order = 9
    )]
    pairings: String,

    #[arg(long, help = "Save output to a file")]
    logging: bool,

    #[arg(long, help = "Hide the progress meter")]
    no_progress: bool,
}

pub fn read_file(filepath: &str) -> String {
    let file = File::open(filepath).expect("could not open file");
    let mut buffered_reader = BufReader::new(file);
    let mut contents = String::new();
    let _number_of_bytes: usize = buffered_reader
        .read_to_string(&mut contents)
        .unwrap_or(0);

    contents
}

fn main() {
    let args = Args::parse();

    let mut permutation_chance = args.permutation_chance;
    while permutation_chance > 1.0 {
        permutation_chance /= 100.0;
    }

    let char_weights: Vec<CharWeight> =
        serde_json::from_str(&read_file(&args.char_weights)).unwrap();
    let character_set: HashSet<char> = char_weights
        .clone()
        .into_iter()
        .map(|c| c.character)
        .collect();

    let key_weights: Vec<Vec<KeyWeight>> =
        serde_json::from_str(&read_file(&args.key_weights)).unwrap();

    let row_sizes: Vec<usize> = key_weights.iter().map(|row| row.len()).collect();

    let total_chars = row_sizes.iter().sum::<usize>() * args.layers;
    if char_weights.len() < total_chars {
        panic!("Fewer characters than keys!");
    } else if char_weights.len() > total_chars {
        warn!("More characters than keys, some will be ignored")
    }

    let ngram_weights: Vec<NGramWeight> =
        serde_json::from_str(&read_file(&args.ngram_weights)).unwrap();

    let pairings: Vec<Pairing> = serde_json::from_str(&read_file(&args.pairings)).unwrap();

    let mut population: Vec<(Layout, f32)> = Vec::new();
    for _ in 0..args.population {
        population.push((
            Layout::initialize(args.layers, &row_sizes, &character_set, &pairings),
            0.0,
        ));
    }

    let progress = if args.no_progress {
        ProgressBar::hidden()
    } else {
        ProgressBar::new(args.generations as u64)
    };

    progress.set_style(
        ProgressStyle::default_bar()
            .template("Generation {pos} of {len}, ≈{eta} remaining\n{bar:100}")
            .unwrap(),
    );
    for _ in 0..args.generations {
        progress.inc(1);

        population = evaluate_and_sort(population, &char_weights, &ngram_weights);
        let top = population[0..args.keep_top_n].to_vec();

        population = (0..args.population)
            .into_par_iter()
            .map(|_| -> (Layout, f32) {
                let rng = &mut thread_rng();

                let rand_layout_a = &top.choose(rng).unwrap().0;
                let rand_layout_b = &top.choose(rng).unwrap().0;
                let new_layout = rand_layout_a.generate(rand_layout_b, permutation_chance);
                (new_layout, 0.0)
            })
            .collect();
    }
    progress.finish();

    population = evaluate_and_sort(population, &char_weights, &ngram_weights);
    let best = &population[0].0;

    println!("{}", best.to_string_pretty());
}

fn evaluate_and_sort(
    mut population: Vec<(Layout, f32)>,
    char_weights: &[CharWeight],
    ngram_weights: &[NGramWeight],
) -> Vec<(Layout, f32)> {
    population.par_iter_mut().for_each(|layout_weight| {
        layout_weight.1 = evaluate(&layout_weight.0, char_weights, ngram_weights);
    });

    population.par_sort_unstable_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

    population
}

fn evaluate(
    _layout: &Layout,
    _char_weights: &[CharWeight],
    _ngram_weights: &[NGramWeight],
) -> f32 {
    //TODO: implement
    0.0
}
