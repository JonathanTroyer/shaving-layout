use super::finger::FingerUse;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct KeyWeight {
    pub finger: FingerUse,
    pub weight: f32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CharWeight {
    pub character: char,
    pub weight: f32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NGramWeight {
    pub ngram: String,
    pub weight: f32,
}
