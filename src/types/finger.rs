use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub enum Hand {
    Left,
    Right,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Finger {
    Index,
    Middle,
    Ring,
    Pinky,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FingerUse {
    pub hand: Hand,
    pub finger: Finger,
}
