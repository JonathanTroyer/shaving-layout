use serde::{Deserialize, Serialize};

pub type Pairing = Vec<char>;

#[derive(Serialize, Deserialize, Debug)]
pub struct Pin {
    pub character: char,
    pub layer: usize,
    pub row: usize,
    pub column: usize
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Fence {
    pub character: char,
    pub layer: Option<usize>,
    pub row: Option<usize>,
    pub column: Option<usize>
}