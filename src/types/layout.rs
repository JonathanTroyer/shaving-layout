use std::collections::HashSet;

use super::constraints::*;
use rand::prelude::*;

pub type Layout = Vec<Vec<Vec<char>>>;

pub trait LayoutTraits {
    fn initialize(
        layers: usize,
        row_sizes: &Vec<usize>,
        character_set: &HashSet<char>,
        pairings: &Vec<Pairing>,
    ) -> Self;
    fn generate(&self, other: &Layout, permutation_chance: f64) -> Layout;
    fn swap(&mut self, row_a: usize, col_a: usize, row_b: usize, col_b: usize);
    fn to_string_pretty(&self) -> String;
}

//TODO: key pairings (i.e. lowercase and uppercase)
//TODO: key pinning (key must be in position)
//TODO: key fencing (key must be in row, col, and/or layer bounds)
impl LayoutTraits for Layout {
    fn initialize(
        layers: usize,
        row_sizes: &Vec<usize>,
        character_set: &HashSet<char>,
        pairings: &Vec<Pairing>,
    ) -> Self {
        let mut rng = thread_rng();

        //Generate an empty layout
        let mut new_layout = Layout::with_capacity(layers);
        for _ in 0..layers {
            let mut new_layer = Vec::with_capacity(row_sizes.len());
            for row_size in row_sizes {
                let mut new_row = Vec::with_capacity(*row_size);
                for _ in 0..*row_size {
                    new_row.push(' ');
                }
                new_layer.push(new_row);
            }
            new_layout.push(new_layer);
        }

        let mut chars_left = character_set.clone();

        //TODO: pinning

        //Satisfy remaining pairings
        for pairing in pairings {
            let rand_row = rng.gen_range(0..row_sizes.len());
            let rand_col = rng.gen_range(0..row_sizes[rand_row]);
            for (layer, c) in pairing.iter().enumerate() {
                new_layout[layer][rand_row][rand_col] = *c;

                chars_left.remove(c);
            }
        }

        //TODO: fencing

        //Fill in remaining characters
        for layer in new_layout.iter_mut() {
            if chars_left.is_empty() {
                break;
            }
            for row in layer.iter_mut() {
                if chars_left.is_empty() {
                    break;
                }
                for col in row.iter_mut() {
                    if chars_left.is_empty() {
                        break;
                    }
                    if *col == ' ' {
                        let rand_char = chars_left.iter().cloned().next().unwrap();
                        *col = rand_char;
                        chars_left.remove(&rand_char);
                    }
                }
            }
        }

        new_layout
    }

    fn generate(&self, other: &Layout, permutation_chance: f64) -> Layout {
        let rng = &mut thread_rng();

        let num_layers = self.len();
        let mut new_layout = Layout::with_capacity(self.len());
        for layer in self {
            let mut new_layer = Vec::with_capacity(layer.len());
            for row in layer {
                let mut new_row = Vec::with_capacity(row.len());
                for _ in row {
                    new_row.push(' ');
                }
                new_layer.push(new_row);
            }
            new_layout.push(new_layer);
        }

        for (row_index, row) in self[0].iter().enumerate() {
            for (col_index, _) in row.iter().enumerate() {
                let rand_layout = if rng.gen::<f64>() < 0.5 { self } else { other };
                for layer_index in 0..num_layers {
                    new_layout[layer_index][row_index][col_index] =
                        rand_layout[layer_index][row_index][col_index];
                }
            }
        }

        while rng.gen::<f64>() < permutation_chance {
            let row_a = rng.gen_range(0..new_layout[0].len());
            let col_a = rng.gen_range(0..new_layout[0][row_a].len());

            let row_b = rng.gen_range(0..new_layout[0].len());
            let col_b = rng.gen_range(0..new_layout[0][row_b].len());

            new_layout.swap(row_a, col_a, row_b, col_b);
        }

        new_layout
    }

    fn swap(&mut self, row_a: usize, col_a: usize, row_b: usize, col_b: usize) {
        for layer in self.iter_mut() {
            let temp = layer[row_a][col_a];
            layer[row_a][col_a] = layer[row_b][col_b];
            layer[row_b][col_b] = temp;
        }
    }

    fn to_string_pretty(&self) -> String {
        let mut output = String::new();
        for (layer_index, layer) in self.iter().enumerate() {
            output.push_str(&format!("Layer {}\n", layer_index));
            for row in layer {
                for col in row {
                    output.push(*col);
                }
                output.push('\n');
            }
            output.push('\n');
        }

        output.trim_end().to_string()
    }
}
